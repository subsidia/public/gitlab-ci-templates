      CMD="gcloud builds submit . --config=$CLOUDBUILD"
      SUBSTITUTIONS=""

      for variable in "${!SUB_@}"
      do
        name=${variable#SUB_}
        value=${!variable}
        SUBSTITUTIONS="$SUBSTITUTIONS,_${name}=$value"
      done

      if [ ! -z "$SUBSTITUTIONS" ]
      then
        SUBSTITUTIONS="--substitutions $SUBSTITUTIONS"
      fi

echo $SUBSTITUTIONS
